# RE_SA

## 1. Setup Tarantool and Clickhouse

```
$ make docker.start.components
```

Tarantool client:

```
$ make tarantool.client
```

Clickhouse client:

[Open this link on browser](http://localhost:8123/play)

## 2. Build Tarantool schema

```shell
$ make build.schema.tarantool
```

## 3. Build Clickhouse schema

```shell
$ make build.schema.clickhouse
```

---

## API List

### - Upsert player on tarantool DB

```shell
## Will automatically insert a row into player log
POST {host}/player

## request body example
{
    "id": 7,
    "name": "Dasamuka",
    "age": 3001
}
```

### - Upsert player log on tarantool DB

```shell
POST {host}/playerlog

## request body example
{
    "dataBefore": "exampleBefore",
    "dataAfter": "exampleAfter"
}
```

### - Get Player by ID with last 10 of its log

```shell
GET {host}/player/:id

## example
$ curl localhost:3000/player/6
```

### - Get Player logs with limit and offset

```shell
GET {host}/playerlog/:limit/:offset

## example
$ curl localhost:3000/playerlog/10/5
```

---

## Run Test

```shell
$ make test.integration

## verbose
$ make test.integration.debug
```
