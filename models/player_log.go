package models

import (
	"time"
)

// PlayerLog struct
type PlayerLog struct {
	CurrentTime time.Time `json:"currentTime,omitempty"`
	UserAgent   string    `json:"userAgent,omitempty"`
	IpAddress   string    `json:"ipAddress,omitempty"`
	DataBefore  string    `json:"dataBefore"`
	DataAfter   string    `json:"dataAfter"`
}
