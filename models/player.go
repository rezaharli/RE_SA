package models

import (
	"gitlab.com/rezaharli/RE_SA/utils"
	"gopkg.in/vmihailenco/msgpack.v2"
)

// Player struct
type Player struct {
	ID   uint   `json:"id,omitempty" bson:"_id,omitempty"`
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func (m *Player) EncodeMsgpack(e *msgpack.Encoder) error {
	if err := e.EncodeSliceLen(3); err != nil {
		return err
	}

	if err := e.EncodeUint(m.ID); err != nil {
		return err
	}

	if err := e.EncodeString(m.Name); err != nil {
		return err
	}

	if err := e.EncodeInt(m.Age); err != nil {
		return err
	}

	return nil
}

func (m *Player) DecodeMsgpack(d *msgpack.Decoder) error {
	l, err := d.DecodeArrayLen()
	if err != nil {
		return err
	}

	decodedFields := 1
	if m.ID, err = d.DecodeUint(); err != nil || decodedFields == l {
		return err
	}

	decodedFields++
	if m.Name, err = d.DecodeString(); err != nil || decodedFields == l {
		return err
	}

	decodedFields++
	if m.Age, err = d.DecodeInt(); err != nil || decodedFields == l {
		return err
	}

	for i := 0; i < l-decodedFields; i++ {
		_ = d.Skip()
	}

	return nil
}

func (m *Player) ToString() (string, error) {
	return utils.StructToString(m)
}
