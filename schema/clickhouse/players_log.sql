-- create table named players_log with ReplacingMergeTree engine
CREATE TABLE IF NOT EXISTS re_sa.players_log (
  `currentTime` DateTime,
  `userAgent` String,
  `ipAddress` String,
  `dataBefore` String,
  `dataAfter` String
) ENGINE = ReplacingMergeTree()
ORDER BY
  currentTime;