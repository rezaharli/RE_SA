-- create space named players
s = box.schema.space.create('players')

-- format the players space
s:format({
  {name = 'id', type = 'unsigned'},
  {name = 'name', type = 'string'},
  {name = 'age', type = 'integer'}
})

-- creating index
s:create_index('id_index', {
  parts = {'id'},
  type = 'hash',
  unique = true,
})

s:create_index('name_index', {
  parts = {'name'},
  type = 'tree',
  unique = false,
})