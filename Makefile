# this command will start a docker components that we set in docker-compose.yml
docker.start.components:
	docker-compose up -d;

# shutting down docker components
docker.stop:
	docker-compose down;

# open tarantool client
tarantool.client:
	docker exec -it re_sa_tarantool_1 console

# build schema for tarantool
build.schema.tarantool:
	docker exec -i re_sa_tarantool_1 console -t < schema/tarantool/players.lua

# build schema for clickhouse
build.schema.clickhouse:
	echo 'CREATE DATABASE IF NOT EXISTS re_sa;' | sh clickhouse-client.sh && cat schema/clickhouse/players_log.sql | sh clickhouse-client.sh

# this command will trigger integration test
test.integration:
	go test -tags=integration ./ -count=1

# this command will trigger integration test with verbose mode
test.integration.debug:
	go test -tags=integration ./ -count=1 -v