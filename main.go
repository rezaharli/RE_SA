package main

import (
	"log"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/rezaharli/RE_SA/handlers"
	"gitlab.com/rezaharli/RE_SA/utils"
)

func main() {
	// Connect to the database
	if err := utils.Connect(); err != nil {
		log.Fatal(err)
	}

	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	})

	app.Post("/player", handlers.UpsertPlayer)
	app.Get("/player/:param", handlers.GetPlayerWithLog)

	app.Post("/playerlog", handlers.UpsertPlayerLog)
	app.Get("/playerlog/:limit/:offset", handlers.GetPlayerLog)

	log.Fatal(app.Listen(":3000"))
}
