package utils

import "encoding/json"

func StructToString(m interface{}) (string, error) {
	marshalled, err := json.Marshal(m)
	if err != nil {
		return "", err
	}

	return string(marshalled[:]), nil
}
