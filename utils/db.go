package utils

import (
	"database/sql"

	_ "github.com/ClickHouse/clickhouse-go"
	"github.com/tarantool/go-tarantool"
)

// DBInstance contains the Tarantool client and database objects
type DBInstance struct {
	TarantoolConn  *tarantool.Connection
	ClickHouseConn *sql.DB
}

var Db DBInstance

// Connect configures the Tarantool and Clickhouse client and initializes the database connection.
func Connect() error {
	tarantoolConn, err := ConnectTarantool()
	if err != nil {
		return err
	}

	clickhouseConn, err := ConnectClickhouse()
	if err != nil {
		return err
	}

	Db = DBInstance{
		TarantoolConn:  tarantoolConn,
		ClickHouseConn: clickhouseConn,
	}

	return nil
}

func ConnectTarantool() (*tarantool.Connection, error) {
	opts := tarantool.Opts{User: "guest"}
	conn, err := tarantool.Connect("localhost:3301", opts)
	if err != nil {
		return nil, err
	}

	return conn, nil
}

func ConnectClickhouse() (*sql.DB, error) {
	conn, err := sql.Open("clickhouse", "tcp://localhost:9000?debug=true")
	if err != nil {
		return nil, err
	}

	if err != nil {
		return nil, err
	}

	return conn, nil
}
