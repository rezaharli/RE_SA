package handlers

import (
	"database/sql"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/rezaharli/RE_SA/models"
	"gitlab.com/rezaharli/RE_SA/utils"
)

const tableName = "re_sa.players_log"

// UpsertPlayerLog updates or inserts playerlog data
func GetPlayerLog(c *fiber.Ctx) error {
	var err error

	limitString := c.Params("limit")
	limitInt, err := strconv.Atoi(limitString)
	if err != nil {
		return c.Status(400).SendString(err.Error())
	}

	offsetString := c.Params("offset")
	offsetInt, err := strconv.Atoi(offsetString)
	if err != nil {
		return c.Status(400).SendString(err.Error())
	}

	// get log of the player
	resp, err := getLimitedPlayerLog(limitInt, offsetInt)
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}

	return c.Status(201).JSON(resp)
}

// UpsertPlayerLog updates or inserts playerlog data
func UpsertPlayerLog(c *fiber.Ctx) error {
	// new PlayerLog struct and parse body into it
	playerLog := new(models.PlayerLog)
	if err := c.BodyParser(playerLog); err != nil {
		return c.Status(400).SendString(err.Error())
	}

	playerLog.CurrentTime = time.Now()
	playerLog.UserAgent = string(c.Request().Header.UserAgent()[:])
	playerLog.IpAddress = c.IP()

	resp, err := upsertPlayerLog(playerLog)
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}

	return c.Status(201).JSON(resp)
}

func getLimitedPlayerLog(limit int, offset int) ([]models.PlayerLog, error) {
	conn := utils.Db.ClickHouseConn

	rows, err := conn.Query(`SELECT * FROM re_sa.players_log ORDER BY currentTime desc LIMIT ` + strconv.Itoa(limit) + ` OFFSET ` + strconv.Itoa(offset))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var result []models.PlayerLog

	for rows.Next() {
		log := models.PlayerLog{}
		var err = rows.Scan(&log.CurrentTime, &log.UserAgent, &log.IpAddress, &log.DataBefore, &log.DataAfter)
		if err != nil {
			return nil, err
		}

		result = append(result, log)
	}

	return result, nil
}

func getLimitedPlayerLogById(id uint, limit int) ([]models.PlayerLog, error) {
	conn := utils.Db.ClickHouseConn

	rows, err := conn.Query(`SELECT * FROM re_sa.players_log WHERE dataBefore LIKE '%"id":` + strconv.Itoa(int(id)) + `%' ORDER BY currentTime desc LIMIT ` + strconv.Itoa(limit))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var result []models.PlayerLog

	for rows.Next() {
		log := models.PlayerLog{}
		var err = rows.Scan(&log.CurrentTime, &log.UserAgent, &log.IpAddress, &log.DataBefore, &log.DataAfter)
		if err != nil {
			return nil, err
		}

		result = append(result, log)
	}

	return result, nil
}

func upsertPlayerLog(playerLog *models.PlayerLog) (sql.Result, error) {
	conn := utils.Db.ClickHouseConn

	var (
		tx, _   = conn.Begin()
		stmt, _ = tx.Prepare("INSERT INTO " + tableName + " (currentTime, userAgent, ipAddress, dataBefore, dataAfter) VALUES (?, ?, ?, ?, ?)")
	)
	defer stmt.Close()

	resp, err := stmt.Exec(
		playerLog.CurrentTime,
		playerLog.UserAgent,
		playerLog.IpAddress,
		playerLog.DataBefore,
		playerLog.DataAfter,
	)
	if err != nil {
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		return nil, err
	}

	return resp, nil
}
