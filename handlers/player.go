package handlers

import (
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/tarantool/go-tarantool"

	"gitlab.com/rezaharli/RE_SA/models"
	"gitlab.com/rezaharli/RE_SA/utils"
)

const spaceName = "players"

// GetPlayer updates or inserts player data
func GetPlayerWithLog(c *fiber.Ctx) error {
	paramString := c.Params("param")
	paramInt, err := strconv.Atoi(paramString)
	if err != nil {
		return c.Status(400).SendString(err.Error())
	}

	ret := map[string]interface{}{}

	// get player
	ret["player"], err = GetPlayerByID(uint(paramInt))
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}

	// get log of the player
	ret["logs"], err = getLimitedPlayerLogById(uint(paramInt), 10)
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}

	return c.Status(201).JSON(ret)
}

// UpsertPlayer updates or inserts player data
func UpsertPlayer(c *fiber.Ctx) error {
	var err error

	// new Player object and parse body into it
	player := new(models.Player)
	if err := c.BodyParser(player); err != nil {
		return c.Status(400).SendString(err.Error())
	}

	// get row before upsert
	dataBefore, err := GetPlayerByID(player.ID)
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}

	// do upsert
	_, err = utils.Db.TarantoolConn.Upsert(spaceName, []interface{}{player.ID, player.Name, player.Age}, []interface{}{[]interface{}{"=", 1, player.Name}, []interface{}{"=", 2, player.Age}})
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}

	// get value after upsert
	dataAfter, err := GetPlayerByID(player.ID)
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}

	// record to log
	playerLog := new(models.PlayerLog)
	playerLog.CurrentTime = time.Now()
	playerLog.UserAgent = string(c.Request().Header.UserAgent()[:])
	playerLog.IpAddress = c.IP()

	playerLog.DataBefore, err = dataBefore.ToString()
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}

	playerLog.DataAfter, err = dataAfter.ToString()
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}

	_, err = upsertPlayerLog(playerLog)
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}

	return c.Status(201).JSON(playerLog)
}

func GetPlayerByID(id uint) (*models.Player, error) {
	var players []models.Player
	err := utils.Db.TarantoolConn.SelectTyped(spaceName, "id_index", 0, 1, tarantool.IterEq, []interface{}{id}, &players)
	if err != nil {
		return nil, err
	}

	if len(players) > 0 {
		return &players[0], nil
	}

	return nil, nil
}
