module gitlab.com/rezaharli/RE_SA

go 1.17

require (
	github.com/ClickHouse/clickhouse-go v1.4.5
	github.com/gofiber/fiber/v2 v2.18.0
	github.com/tarantool/go-tarantool v0.0.0-20210330210617-56fe55c5fa5c
	gopkg.in/vmihailenco/msgpack.v2 v2.9.2
)

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/cloudflare/golz4 v0.0.0-20150217214814-ef862a3cdc58 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/klauspost/compress v1.13.4 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.29.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/net v0.0.0-20210825183410-e898025ed96a // indirect
	golang.org/x/sys v0.0.0-20210823070655-63515b42dcdf // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
