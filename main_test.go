package main_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"

	"gitlab.com/rezaharli/RE_SA/handlers"
	u "gitlab.com/rezaharli/RE_SA/utils"
)

func init() {
	fmt.Println("init")
	if err := u.Connect(); err != nil {
		fmt.Println(err.Error())
	}
}

//TestInsertPlayer tests inserting new player
func TestInsertPlayer(t *testing.T) {
	app := fiber.New()
	app.Post("/player", handlers.UpsertPlayer)

	// insert player with id 7
	r := httptest.NewRequest(http.MethodPost, "/player", strings.NewReader(`{"id": 7, "name": "Dasamuka", "age": 3001}`))
	r.Header.Set("Content-Type", "application/json")
	resp, err := app.Test(r)

	utils.AssertEqual(t, nil, err, "Insert data")
	utils.AssertEqual(t, 201, resp.StatusCode, "Status code")

	// check if player with id 7 exist
	row, err := handlers.GetPlayerByID(7)
	isRow := row != nil

	utils.AssertEqual(t, nil, err, "Check data exist")
	utils.AssertEqual(t, true, isRow, "Row inserted")
}

//TestInsertPlayer tests inserting existed player
func TestUpdatePlayer(t *testing.T) {
	app := fiber.New()
	app.Post("/player", handlers.UpsertPlayer)

	// insert player with id 7
	r := httptest.NewRequest(http.MethodPost, "/player", strings.NewReader(`{"id": 7, "name": "Dasamuka", "age": 3001}`))
	r.Header.Set("Content-Type", "application/json")
	resp, err := app.Test(r)

	utils.AssertEqual(t, nil, err, "Insert data")
	utils.AssertEqual(t, 201, resp.StatusCode, "Status code")

	// insert player with id 7
	r = httptest.NewRequest(http.MethodPost, "/player", strings.NewReader(`{"id": 7, "name": "Anoman", "age": 3002}`))
	r.Header.Set("Content-Type", "application/json")
	resp, err = app.Test(r)

	utils.AssertEqual(t, nil, err, "Update data")
	utils.AssertEqual(t, 201, resp.StatusCode, "Status code")

	// check if player with id 7 exist
	row, err := handlers.GetPlayerByID(7)
	isRow := row != nil

	fmt.Println(row.Name, row.Age)

	utils.AssertEqual(t, nil, err, "Check data exist")
	utils.AssertEqual(t, true, isRow, "Row inserted")
	utils.AssertEqual(t, "Anoman", row.Name, "Name updated")
	utils.AssertEqual(t, 3002, row.Age, "Age updated")
}
